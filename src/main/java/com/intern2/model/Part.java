package com.intern2.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NamedQueries(
        {
                @NamedQuery(name = "Part_getPartsByBrand", query = "from Part where car_type = :brand"),
                @NamedQuery(name = "Part_getAvailableParts", query = "from Part where quantity > 0")
        }
)
@Entity
@Table(name = "part")
public class Part {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;
    private String car_type;
    private double price;
    private int quantity;
    private String condition;
    private int discount;
    private int tva;

    @ManyToMany(mappedBy = "parts")
    private List<Repairment> repairments = new ArrayList<>();

    public Part(String name, String car_type, double price, int quantity, String condition, int discount, int tva) {
        this.name = name;
        this.car_type = car_type;
        this.price = price;
        this.quantity = quantity;
        this.condition = condition;
        this.discount = discount;
        this.tva = tva;
    }

    public Part() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCar_type() {
        return car_type;
    }

    public void setCar_type(String car_type) {
        this.car_type = car_type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getTva() {
        return tva;
    }

    public void setTva(int tva) {
        this.tva = tva;
    }

    public List<Repairment> getRepairments() {
        return repairments;
    }

    public void setRepairments(List<Repairment> repairments) {
        this.repairments = repairments;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Repairment repairment : repairments) {
            stringBuilder.append(repairment.getId()).append(", ");
        }
        return "Part{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", car_type='" + car_type + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", condition='" + condition + '\'' +
                ", discount=" + discount +
                ", tva=" + tva +
                ", repairments=" + stringBuilder +
                '}';
    }
}
