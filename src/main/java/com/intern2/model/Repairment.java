package com.intern2.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NamedQueries(
        {
                @NamedQuery(name = "Repairment_getRepairmentsByDay", query = "from Repairment where repairment_date = :repDate"),
                @NamedQuery(name = "Repairment_getRepairmentsByCarId", query = "from Repairment where car_id = :carId")
        }
)

@Entity
@Table(name = "repairment")
public class Repairment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Temporal(TemporalType.DATE)
    @Column(name = "repairment_date")
    private Date repairment_date;

    private int car_id;
    private String review;
    private int user_id;
    private int workmanship_price;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    }, fetch = FetchType.EAGER)
    @JoinTable(name = "repairment_part",
            joinColumns = @JoinColumn(name = "repairment_id"),
            inverseJoinColumns = @JoinColumn(name = "part_id"))
    private List<Part> parts = new ArrayList<>();

    public Repairment(Date repairment_date, int car_id, String review, int user_id, int workmanship_price) {
        this.repairment_date = repairment_date;
        this.car_id = car_id;
        this.review = review;
        this.user_id = user_id;
        this.workmanship_price = workmanship_price;
    }

    public Repairment() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getRepairment_date() {
        return repairment_date;
    }

    public void setRepairment_date(Date repairment_date) {
        this.repairment_date = repairment_date;
    }

    public int getCar_id() {
        return car_id;
    }

    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getWorkmanship_price() {
        return workmanship_price;
    }

    public void setWorkmanship_price(int workmanship_price) {
        this.workmanship_price = workmanship_price;
    }

    public List<Part> getParts() {
        return parts;
    }

    public void setParts(List<Part> parts) {
        this.parts = parts;
    }

    public void addPartToRepairment(Part part) {
        this.parts.add(part);
        part.getRepairments().add(this);
    }

    public void removePartFromRepairment(Part part) {
        this.parts.remove(part);
        part.getRepairments().remove(this);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Part part : parts) {
            stringBuilder.append(part.getName()).append(", ");
        }
        return "Repairment{" +
                "id=" + id +
                ", repairment_date=" + repairment_date +
                ", car_id=" + car_id +
                ", review='" + review + '\'' +
                ", user_id=" + user_id +
                ", workmanship_price=" + workmanship_price +
                ", parts=" + stringBuilder +
                '}';
    }
}
