package com.intern2.model;

import javax.persistence.*;

@NamedQueries(
        {
                @NamedQuery(name = "Car_getCarsByOwnerId", query = "from Car where owner_id = :ownerId")
        }
)
@Entity
@Table(name = "car")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String brand;
    private String registration_number;
    private String status;
    private int owner_id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_id", updatable = false, insertable = false)
    private User user;

    public Car(String brand, String registration_number, String status, int owner_id) {
        this.brand = brand;
        this.registration_number = registration_number;
        this.status = status;
        this.owner_id = owner_id;
    }

    public Car() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(int owner_id) {
        this.owner_id = owner_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", registration_number='" + registration_number + '\'' +
                ", status='" + status + '\'' +
                ", owner_id=" + owner_id +
                ", user=" + user.getName() +
                '}';
    }
}
