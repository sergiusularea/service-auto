package com.intern2.service;

import com.intern2.model.Car;
import com.intern2.model.Repairment;
import com.intern2.model.User;
import com.intern2.validations.NumberValidation;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;

import javax.persistence.TypedQuery;
import java.awt.*;
import java.sql.SQLOutput;
import java.util.List;
import java.util.Scanner;

public class CarService {
    private final Configuration configuration;
    private final StandardServiceRegistryBuilder builder;
    private final SessionFactory factory;

    public CarService(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        this.configuration = configuration;
        this.builder = builder;
        this.factory = factory;
    }

    public boolean createCar(Car insertedCar) {
        boolean carHasBeenInserted = false;
        UserService userService = new UserService(configuration, builder, factory);
        User searchedUser = userService.getUserById(insertedCar.getOwner_id());
        if (searchedUser != null) {
            Session session = factory.openSession();
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                session.save(insertedCar);
                tx.commit();
                carHasBeenInserted = true;
            } catch (HeadlessException e) {
                if (tx != null) tx.rollback();
                e.printStackTrace();
            } finally {
                session.close();
            }
        }
        return carHasBeenInserted;
    }

    public void updateCar(int id, String brand, String registration_number, String status, int owner_id) {
        UserService userService = new UserService(configuration, builder, factory);
        User searchedUser = userService.getUserById(owner_id);
        if (searchedUser != null) {
            Session session = factory.openSession();
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                Car updatedCar = session.get(Car.class, id);
                if (updatedCar != null) {
                    updatedCar.setOwner_id(owner_id);
                    updatedCar.setBrand(brand);
                    updatedCar.setRegistration_number(registration_number);
                    updatedCar.setStatus(status);
                    session.update(updatedCar);
                } else {
                    System.out.println("Car with id = " + id + " does not exist");
                }
                tx.commit();
            } catch (HibernateException e) {
                if (tx != null) tx.rollback();
                e.printStackTrace();
            } finally {
                session.close();
            }
        } else {
            System.out.println("Owner with id = " + owner_id + " does not exist");
        }
    }

    public Car getCarById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Car returnedCar = null;
        try {
            tx = session.beginTransaction();
            returnedCar = session.get(Car.class, id);
            if (returnedCar == null) {
                System.out.println("Car with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedCar;
    }

    public List<Car> getAllCars() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Car> returnedCars = null;
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Car.class);
            cr.addOrder(Order.asc("id"));
            returnedCars = cr.list();
//            for (Object object : returnedCars) {
//                Car car = (Car) object;
//                System.out.println(car.toString());
//            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedCars;
    }

    public Car deleteUserById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Car deletedCar = null;
        try {
            tx = session.beginTransaction();
            deletedCar = session.get(Car.class, id);
            if (deletedCar != null) {
                session.delete(deletedCar);
            } else {
                System.out.println("Car with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return deletedCar;
    }

    public List<Car> getCarByOwnerId(int owner_id) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Car> returnedCars = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("Car_getCarsByOwnerId");
            query.setParameter("ownerId", owner_id);
            returnedCars = query.getResultList();
            if (!returnedCars.isEmpty()) {
                for (Object e : returnedCars) {
                    System.out.println(e);
                }
            } else {
                System.out.println("User with id = " + owner_id + " does not have any Cars");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedCars;
    }

    public void displayAllCarsMenuAndChooseFromIt() {
        RepairmentService repairmentService = new RepairmentService(configuration, builder, factory);
        Scanner in = new Scanner(System.in);
        List<Car> cars = this.getAllCars();
        while (true) {
            System.out.println("All cars registered at the service : ");
            for (int i = 0; i < cars.size(); i++) {
                System.out.println(i + 1 + ". " + cars.get(i).getBrand() + " " + cars.get(i).getRegistration_number());
            }
            System.out.println(cars.size() + 1 + ". Exit");
            System.out.println("Choose a car to display its history : ");
            String choice = in.nextLine();
            int option;
            try {
                option = Integer.parseInt(choice);
                if (option == cars.size() + 1) {
                    return;
                } else if (option < 1 || option > cars.size()) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException ex) {
                System.out.println("Invalid choice. Try again");
                continue;
            }
            Car choosedCar = cars.get(option - 1);
            System.out.println(choosedCar);
            System.out.println("All repairments which have been made for this car are : ");
            List<Repairment> repairmentsOfACar = repairmentService.getRepairmentsByCar(choosedCar.getId());
            while (true) {
                for (int i = 0; i < repairmentsOfACar.size(); i++) {
                    Repairment repairmentIterator = repairmentsOfACar.get(i);
                    System.out.println(i + 1 + ". " + repairmentIterator.getRepairment_date());
                }
                System.out.println(repairmentsOfACar.size() + 1 + ". Exit");
                System.out.println("Choose a repairment to display its details : ");
                choice = in.nextLine();
                try {
                    option = Integer.parseInt(choice);
                    if (option == repairmentsOfACar.size() + 1) {
                        return;
                    } else if (option < 1 || option > repairmentsOfACar.size()) {
                        throw new NumberFormatException();
                    }
                } catch (NumberFormatException ex) {
                    System.out.println("Invalid choice. Try again");
                    continue;
                }
                System.out.println(repairmentsOfACar.get(option - 1));
            }
        }
    }

    public Car insertCarPropertiesAndCreateCar() {
        Scanner in = new Scanner(System.in);
        System.out.println("Insert brand of the car: ");
        String brand = in.nextLine();
        System.out.print("\nInsert registration number of the car : ");
        String registrationNumber = in.nextLine();
        System.out.print("\nInsert status (registered , in work , repaired) of the car: ");
        String status = in.nextLine();
        System.out.print("\nInsert owner id of the car : ");
        int ownerId = NumberValidation.parseStringToInt(in.nextLine());
        Car insertedCar = null;
        while (true) {
            insertedCar = new Car(brand, registrationNumber, status, ownerId);
            boolean inserted = this.createCar(insertedCar);
            if (!inserted) {
                System.out.println("Try again another owner id ");
                ownerId = NumberValidation.parseStringToInt(in.nextLine());
            } else break;
        }
        return insertedCar;
    }
}
