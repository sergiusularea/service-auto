package com.intern2.service;

import com.intern2.model.Car;
import com.intern2.model.Part;
import com.intern2.model.Repairment;
import com.intern2.model.User;
import com.intern2.utils.DateUtil;
import com.intern2.validations.NumberValidation;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;

import javax.persistence.TypedQuery;
import java.awt.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class RepairmentService {
    private final Configuration configuration;
    private final StandardServiceRegistryBuilder builder;
    private final SessionFactory factory;
    private final CarService carService;
    private final UserService userService;

    public RepairmentService(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        this.configuration = configuration;
        this.builder = builder;
        this.factory = factory;
        carService = new CarService(configuration, builder, factory);
        userService = new UserService(configuration, builder, factory);
    }

    public boolean createRepairment(Repairment repairmentToBeInserted) {
        boolean repairmentHasBeenInserted = false;
        Session session = factory.openSession();
        User searchedUser = userService.getUserById(repairmentToBeInserted.getUser_id());
        Car searchedCar = carService.getCarById(repairmentToBeInserted.getCar_id());
        if (searchedCar != null && searchedUser != null) {
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
//                insertedRepairment = new Repairment(repairment_date, car_id, review, user_id, workmanship_price);
                session.save(repairmentToBeInserted);
                tx.commit();
                repairmentHasBeenInserted = true;
            } catch (HeadlessException e) {
                if (tx != null) tx.rollback();
                e.printStackTrace();
            } finally {
                session.close();
            }
        }
        return repairmentHasBeenInserted;
    }

    public void updateRepairment(int id, Date repairment_date, int car_id, String review, int user_id, int workmanship_price) {

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Repairment updatedRepairment = session.get(Repairment.class, id);
            if (updatedRepairment != null) {
                updatedRepairment.setRepairment_date(repairment_date);
                updatedRepairment.setCar_id(car_id);
                updatedRepairment.setReview(review);
                updatedRepairment.setUser_id(user_id);
                updatedRepairment.setWorkmanship_price(workmanship_price);
                session.update(updatedRepairment);
            } else {
                System.out.println("Repairment with id = " + id + " does not exist");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Repairment getRepairmentById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Repairment returnedRepairment = null;
        try {
            tx = session.beginTransaction();
            returnedRepairment = session.get(Repairment.class, id);
            if (returnedRepairment != null) {
                System.out.println(returnedRepairment);
            } else {
                System.out.println("Repairment with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedRepairment;
    }

    public List<Repairment> getAllRepairments() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Repairment> returnedRepairments = null;
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Repairment.class);
            cr.addOrder(Order.asc("id"));
            returnedRepairments = cr.list();
            for (Object object : returnedRepairments) {
                Repairment repairment = (Repairment) object;
                System.out.println(repairment.toString());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedRepairments;
    }

    public Repairment deleteRepairmentById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Repairment deletedRepairment = null;
        try {
            tx = session.beginTransaction();
            deletedRepairment = session.get(Repairment.class, id);
            if (deletedRepairment != null) {
                session.delete(deletedRepairment);
            } else {
                System.out.println("Repairment with id= " + id + " does not exist !");
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return deletedRepairment;
    }

    public List<Repairment> getRepairmentsByDay(Date repDate) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Repairment> returnedRepairments = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("Repairment_getRepairmentsByDay");
            query.setParameter("repDate", repDate);
            returnedRepairments = query.getResultList();
            if (!returnedRepairments.isEmpty()) {
//                for (Object e : returnedParts) {
//                    System.out.println(e);
//                }
            } else {
                System.out.println("There are no repairments in " + repDate);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedRepairments;
    }

    public void registerCarAddPartsToItAndAddRepairmentForUser(User loggedUser) throws ParseException {
        PartService partService = new PartService(configuration, builder, factory);
        Scanner in = new Scanner(System.in);
        int totalPriceOfRepairment = 0;
        Car insertedCar = carService.insertCarPropertiesAndCreateCar();
        List<Part> allParts = partService.getPartsByBrand(insertedCar.getBrand());
        List<Part> usedParts = new ArrayList<>();
        String repDate;
        int workmanshipPrice;
        String review;
        while (true) {
            System.out.println("Choose which parts have been used for repairment : ");
            for (int i = 0; i < allParts.size(); i++) {
                System.out.println(i + ". " + allParts.get(i).getName());
            }
            System.out.println(allParts.size() + ". Exit");
            int choice = NumberValidation.parseStringToInt(in.nextLine());
            if (choice < allParts.size()) {
                Part choosedPart = allParts.get(choice);
                if (choosedPart != null) {
                    if (choosedPart.getQuantity() > 0) {
                        totalPriceOfRepairment += choosedPart.getPrice();
                        usedParts.add(choosedPart);
                        System.out.println(choosedPart.getName() + " was added to the total repairment bill");
                        partService.updatePart(choosedPart.getId(), choosedPart.getName(), choosedPart.getCar_type(), choosedPart.getPrice(), choosedPart.getQuantity() - 1, choosedPart.getCondition(), choosedPart.getDiscount(), choosedPart.getTva());
                    } else if (choosedPart.getQuantity() == 0) {
                        //nu pot sterge piesa
                        System.out.println("Repairment part = " + choosedPart.getName() + " is not available in stock");
//                                    partService.deletePartById(choosedPart.getId());
                    }
                }
            } else {
                System.out.println("Insert details about repairment ");
                System.out.print("\nInsert reparation date : ");
                repDate = in.nextLine();
                System.out.print("\nInsert workmanship price : ");
                workmanshipPrice = NumberValidation.parseStringToInt(in.nextLine());
                System.out.print("\nInsert review : ");
                review = in.nextLine();
                Repairment repairment = new Repairment(DateUtil.dateFormat(repDate), insertedCar.getId(), review, loggedUser.getId(), workmanshipPrice);
                repairment.setParts(usedParts);
                this.createRepairment(repairment);
                break;
            }
        }
        if (usedParts.isEmpty()) {
            System.out.println("Nothing has been changed for car with registration number = " + insertedCar.getRegistration_number());
        } else {
            System.out.println("All parts which have been used for repairment of car with registration number = " + insertedCar.getRegistration_number() + " :");
            for (int i = 0; i < usedParts.size(); i++) {
                System.out.println(i + ". " + usedParts.get(i).getName() + " costs = " + usedParts.get(i).getPrice());
            }
            System.out.println("Workmanship price = " + workmanshipPrice);
            int totalPrice = totalPriceOfRepairment + workmanshipPrice;
            System.out.println("Total price for repairment = " + totalPrice);
        }
    }

    public List<Repairment> getRepairmentsByCar(int carId) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Repairment> returnedRepairments = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("Repairment_getRepairmentsByCarId");
            query.setParameter("carId", carId);
            returnedRepairments = query.getResultList();
            if (!returnedRepairments.isEmpty()) {
//                for (Object e : returnedParts) {
//                    System.out.println(e);
//                }
            } else {
                System.out.println("There are no repairments for car with id = " + carId);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedRepairments;
    }

    public void displayRepairmentsOfACar(){

    }
}
