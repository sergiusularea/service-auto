package com.intern2.service;

import com.intern2.model.Car;
import com.intern2.model.Part;
import com.intern2.validations.NumberValidation;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;

import javax.persistence.TypedQuery;
import java.awt.*;
import java.util.List;
import java.util.Scanner;

public class PartService {
    private final Configuration configuration;
    private final StandardServiceRegistryBuilder builder;
    private final SessionFactory factory;

    public PartService(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        this.configuration = configuration;
        this.builder = builder;
        this.factory = factory;
    }

    public boolean createPart(Part insertPart) {
        boolean partHasBeenInserted = false;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(insertPart);
            tx.commit();
            partHasBeenInserted = true;
        } catch (HeadlessException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return partHasBeenInserted;
    }

    public void updatePart(int id, String name, String car_type, double price, int quantity, String condition, int discount, int tva) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Part updatedPart = session.get(Part.class, id);
            if (updatedPart != null) {
                updatedPart.setName(name);
                updatedPart.setCar_type(car_type);
                updatedPart.setPrice(price);
                updatedPart.setQuantity(quantity);
                updatedPart.setCondition(condition);
                updatedPart.setDiscount(discount);
                updatedPart.setTva(tva);
                session.update(updatedPart);
            } else {
                System.out.println("Part with id = " + id + " does not exist");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Part getPartById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Part returnedPart = null;
        try {
            tx = session.beginTransaction();
            returnedPart = session.get(Part.class, id);
            if (returnedPart == null) {
                System.out.println("Part with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedPart;
    }

    public List<Part> getAllParts() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Part> returnedParts = null;
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(Part.class);
            cr.addOrder(Order.asc("id"));
            returnedParts = cr.list();
            for (Object object : returnedParts) {
                Part part = (Part) object;
                System.out.println(part.toString());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedParts;
    }

    public Part deletePartById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Part deletedPart = null;
        try {
            tx = session.beginTransaction();
            deletedPart = session.get(Part.class, id);
            if (deletedPart != null) {
                session.delete(deletedPart);
            } else {
                System.out.println("Part with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return deletedPart;
    }

    public List<Part> getPartsByBrand(String brand) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Part> returnedParts = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("Part_getPartsByBrand");
            query.setParameter("brand", brand);
            returnedParts = query.getResultList();
            if (!returnedParts.isEmpty()) {
//                for (Object e : returnedParts) {
//                    System.out.println(e);
//                }
            } else {
                System.out.println("Parts with brand = " + brand + " does exist");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedParts;
    }

    public void insertPartPropertiesAndCreatePart() {
        Scanner in = new Scanner(System.in);
        System.out.print("Insert name of the repairment part : ");
        String name = in.nextLine();
        System.out.print("\nInsert car type : ");
        String car_type = in.nextLine();
        System.out.print("\nInsert price : ");
        double price = NumberValidation.parseStringToDouble(in.nextLine());
        System.out.print("\nInsert quantity : ");
        int quantity = NumberValidation.parseStringToInt(in.nextLine());
        System.out.print("\nInsert condition (new, used): ");
        String condition = in.nextLine();
        System.out.print("\nInsert discount : ");
        int discount = NumberValidation.parseStringToInt(in.nextLine());
        System.out.print("\nInsert tva : ");
        int tva = NumberValidation.parseStringToInt(in.nextLine());
        Part insertedPart = new Part(name, car_type, price, quantity, condition, discount, tva);
        this.createPart(insertedPart);
        System.out.println("Repairment part added successfully");
    }

    public void displayAllAvailableParts() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Part> returnedParts = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("Part_getAvailableParts");
            returnedParts = query.getResultList();
            if (!returnedParts.isEmpty()) {
                for (Part part : returnedParts) {
                    System.out.println(part.toString());
                }
            } else {
                System.out.println("There are no available parts in stock");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}