package com.intern2.service;

import com.intern2.model.Part;
import com.intern2.model.User;
import com.intern2.validations.NumberValidation;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Scanner;


public class UserService {
    private final Configuration configuration;
    private final StandardServiceRegistryBuilder builder;
    private final SessionFactory factory;

    public UserService(Configuration configuration, StandardServiceRegistryBuilder builder, SessionFactory factory) {
        this.configuration = configuration;
        this.builder = builder;
        this.factory = factory;
    }

    public boolean createUser(User insertedUser) {
        boolean userHasBeenInserted = false;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(insertedUser);
            tx.commit();
            userHasBeenInserted = true;
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return userHasBeenInserted;
    }

    public List<User> getAllUsers() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<User> returnedUsers = null;
        try {
            tx = session.beginTransaction();
            Criteria cr = session.createCriteria(User.class);
            cr.addOrder(Order.asc("id"));
            returnedUsers = cr.list();
            for (Object object : returnedUsers) {
                User user = (User) object;
                System.out.println(user.toString());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedUsers;
    }

    public void updateUser(int id, String name, String email, String password, String role, String phone) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            User updateUser = session.get(User.class, id);
            if (updateUser != null) {
                updateUser.setEmail(email);
                updateUser.setName(name);
                updateUser.setPassword(password);
                updateUser.setPhone(phone);
                updateUser.setRole(role);
                session.update(updateUser);
            } else {
                System.out.println("User with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public User getUserById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        User returnedUser = null;
        try {
            tx = session.beginTransaction();
            returnedUser = session.get(User.class, id);
            if (returnedUser == null) {
                System.out.println("User with id= " + id + " does not exist !");
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedUser;
    }

    public User deleteUserById(int id) {
        Session session = factory.openSession();
        Transaction tx = null;
        User deletedUser = null;
        try {
            tx = session.beginTransaction();
            deletedUser = session.get(User.class, id);
            if (deletedUser != null) {
                session.delete(deletedUser);
            } else {
                System.out.println("User with id= " + id + " does not exist !");
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return deletedUser;
    }

    public List<User> getUsersByEmailAndPass(String email, String password) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<User> returnedUsers = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("User_getUsersByEmailAndPassword");
            query.setParameter("userEmail", email);
            query.setParameter("userPassword", password);
            returnedUsers = query.getResultList();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedUsers;
    }

    public List<User> getUsersByEmail(String email) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<User> returnedUsers = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("User_getUsersByEmail");
            query.setParameter("userEmail", email);
            returnedUsers = query.getResultList();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedUsers;
    }

    public List<User> getUsersByPassword(String password) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<User> returnedUsers = null;
        try {
            tx = session.beginTransaction();
            TypedQuery query = session.getNamedQuery("User_getUsersByPassword");
            query.setParameter("userPassword", password);
            returnedUsers = query.getResultList();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return returnedUsers;
    }

    public String checkIfInputExistsInDbAsEmail() {
        Scanner in = new Scanner(System.in);
        String username;
        System.out.println("Type username : ");
        username = in.nextLine();
        while (true) {
            List<User> userList = this.getUsersByEmail(username);
            if (userList.size() == 0) {
                System.out.println("Invalid username . Try again ");
                System.out.println("Type username : ");
                username = in.nextLine();
            } else break;
        }
        return username;
    }

    public String checkIfInputExistsInDbAsPassword() {
        Scanner in = new Scanner(System.in);
        String password;
        System.out.println("Type password : ");
        password = in.nextLine();
        while (true) {
            List<User> userList = this.getUsersByPassword(password);
            if (userList.size() == 0) {
                System.out.println("Invalid password . Try again ");
                System.out.println("Type password : ");
                password = in.nextLine();
            } else break;
        }
        return password;
    }
}
