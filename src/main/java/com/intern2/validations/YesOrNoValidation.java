package com.intern2.validations;

public class YesOrNoValidation {
    public static boolean validateInput(String input) {
        String choice = input.toLowerCase();
        String check = null;
        boolean result;
        if (choice.equals("no")) {
            result = true;
            check = "false";
        } else if (choice.equals("yes")) {
            result = true;
            check = "true";
        } else {
            result = false;
            System.out.printf("\"%s\" is not a valid choice. Choose yes/no\n", choice);
        }

        return result;
    }
}
