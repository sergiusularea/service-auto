package com.intern2.validations;

import java.util.Scanner;

public class NumberValidation {

    public static int parseStringToInt(String inputAsString) {
        int parsedNumber;
        Scanner in = new Scanner(System.in);
        while (true) {
            try {
                parsedNumber = Integer.parseInt(inputAsString);
                break;
            } catch (NumberFormatException ex) {
                System.out.println("Invalid number. Try again : ");
                inputAsString = in.nextLine();
            }
        }
        return parsedNumber;
    }

    public static double parseStringToDouble(String inputAsString) {
        double parsedNumber;
        Scanner in = new Scanner(System.in);
        while (true) {
            try {
                parsedNumber = Double.parseDouble(inputAsString);
                break;
            } catch (NumberFormatException ex) {
                System.out.println("Invalid number. Try again : ");
                inputAsString = in.nextLine();
            }
        }
        return parsedNumber;
    }
}
