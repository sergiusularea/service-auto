package com.intern2;

import com.intern2.model.Car;
import com.intern2.model.Part;
import com.intern2.model.Repairment;
import com.intern2.model.User;
import com.intern2.service.CarService;
import com.intern2.service.PartService;
import com.intern2.service.RepairmentService;
import com.intern2.service.UserService;
import com.intern2.validations.NumberValidation;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static final Configuration configuration = new Configuration().addAnnotatedClass(Car.class).addAnnotatedClass(Part.class).addAnnotatedClass(Repairment.class).addAnnotatedClass(User.class).configure();
    private static final StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
    private static final SessionFactory factory = configuration.buildSessionFactory(builder.build());

    public static void main(String[] args) throws ParseException {
        PartService partService = new PartService(configuration, builder, factory);
        RepairmentService repairmentService = new RepairmentService(configuration, builder, factory);
        UserService userService = new UserService(configuration, builder, factory);
        CarService carService = new CarService(configuration, builder, factory);
        Scanner in = new Scanner(System.in);
        System.out.println("Please login to continue ");
        String username = userService.checkIfInputExistsInDbAsEmail();
        String password = userService.checkIfInputExistsInDbAsPassword();
        List<User> userByEmailAndPass = userService.getUsersByEmailAndPass(username, password);
        if (userByEmailAndPass.size() > 0) {
            User loggedUser = userByEmailAndPass.get(0);
            while (true) {
                System.out.println("Choose an option : ");
                System.out.print("1 - Add a repairment part into the DB\n2 - Register a car\n3 - Display all cars registered\n4 - Display all available parts\n0 - Exit\n");
                String optionString = in.nextLine();
                int option = NumberValidation.parseStringToInt(optionString);
                switch (option) {
                    case 1:
                        partService.insertPartPropertiesAndCreatePart();
                        break;
                    case 2:
                        repairmentService.registerCarAddPartsToItAndAddRepairmentForUser(loggedUser);
                        break;
                    case 3:
                        carService.displayAllCarsMenuAndChooseFromIt();
                        break;
                    case 4:
                        partService.displayAllAvailableParts();
                        break;
                    case 0:
                        System.out.println("Exitting...");
                        return;
                }
            }
        } else {
            System.out.println("Could not find the account");
        }
        factory.close();
    }
}
